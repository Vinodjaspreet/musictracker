package com.v.simplelogic.musictracker;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DataBaseHelper extends SQLiteOpenHelper
{
	// Database Name
		static final String DATABASE_NAME = "musictracker.db";
		static final int DATABASE_VERSION = 3;
		public static final int VERSION = 1;


        private static final String TABLE_TRACK_LIST= "tracks";

		 

	    
	public DataBaseHelper(Context context)
    {
    	super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase _db) {
		_db.execSQL(LoginDataBaseAdapter.DATABASE_CREATE_TRACK_LIST);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion)
	{
			// Log the version upgrade.

			onCreate(_db);
	}

	// Getting All Local_Data
	public List<Local_Data> getAllMain() {
		List<Local_Data> contactList = new ArrayList<Local_Data>();
		// Select All Query
		String selectQuery = "SELECT track,artist,album,date1 FROM " + TABLE_TRACK_LIST;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Local_Data contact = new Local_Data();
				contact.setTrack(cursor.getString(0));
				contact.setArtist(cursor.getString(1));
				contact.setAlbum(cursor.getString(2));
				contact.setDate1(cursor.getString(3));
				//contact.setImei(cursor.getString(3));

				// Adding contact to list
				contactList.add(contact);
			} while (cursor.moveToNext());
		}
		// return contact list?
		return contactList;
	}





}
