package com.v.simplelogic.musictracker;


public class DataModel {


    String artist;
    String track;
    String album;
    String daten;
    int image;

    public DataModel(String artist, String track, String album,int image,String daten) {
        this.artist = artist;
        this.track = track;
        this.album = album;
        this.image = image;
        this.daten = daten;

    }


    public String getartist() {
        return artist;
    }


    public String gettrack() {
        return track;
    }

    public int getImage() {
        return image;
    }

    public String getalbum() {
        return album;
    }

    public String getdaten() {
        return daten;
    }
}