package com.v.simplelogic.musictracker;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.util.ArrayList;




public class MyService extends Service  {
	private static final String TAG = "LocationActivity";

	LoginDataBaseAdapter loginDataBaseAdapter;
	ArrayList<String> results = new ArrayList<String>();

	int timeOfDay;

	DataBaseHelper dbvoc = new DataBaseHelper(this);;



	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {


		return START_NOT_STICKY;
	}




	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

//    @Override
//    public void onLocationChanged(Location arg0) {
//        // TODO Auto-generated method stub
//
//    }

	@Override
	public void onDestroy() {
		// I want to restart this service again in one hour
		AlarmManager alarm = (AlarmManager)getSystemService(ALARM_SERVICE);
		alarm.set(
				alarm.RTC_WAKEUP,
				System.currentTimeMillis() + (1000 * 60 * 5),
				PendingIntent.getService(this, 0, new Intent(this, MyService.class), 0)
		);
	}


}