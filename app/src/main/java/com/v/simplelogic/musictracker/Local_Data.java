package com.v.simplelogic.musictracker;

public class Local_Data {
	
	String track;
    String artist ;
    String album;
    String date1;


    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public String getAlbum() {

        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {

        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getTrack() {

        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }
}
